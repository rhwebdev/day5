<?php
   function requestHandler($request)
   {
      $errors = [];
      foreach ($request as $key => $value) {
         if(empty($value)){
            array_push($errors, $key." should be not empty");
         }
      }
      return $errors;
   }
?>